from bonus_system import calculateBonuses

STANDARD = "Standard"
PREMIUM = "Premium"
DIAMOND = "Diamond"

programConfigs = {
    STANDARD: [
        (1, 0.5 * 1),
        (10000, 0.5 * 1.5),
        (100000, 0.5 * 2.5),
        (100001, 0.5 * 2.5),
        (10001, 0.5 * 1.5),
        (50000, 0.5 * 2),
        (50001, 0.5 * 2),
    ],
    PREMIUM: [
        (1, 0.1 * 1),
        (10000, 0.1 * 1.5),
        (100000, 0.1 * 2.5),
        (100001, 0.1 * 2.5),
        (10001, 0.1 * 1.5),
        (50000, 0.1 * 2),
        (50001, 0.1 * 2),
    ],
    DIAMOND: [
        (1, 0.2 * 1),
        (10000, 0.2 * 1.5),
        (100000, 0.2 * 2.5),
        (100001, 0.2 * 2.5),
        (10001, 0.2 * 1.5),
        (50000, 0.2 * 2),
        (50001, 0.2 * 2),
    ],
}


def _test_program(program, configs=programConfigs):
    for input_, output_ in configs[program]:
        assert calculateBonuses(program, input_) == output_


def test_program_standard():
    _test_program(STANDARD)


def test_program_premium():
    _test_program(PREMIUM)


def test_program_diamond():
    _test_program(DIAMOND)


programConfigsIncorrect = [
    ("", 1, 0),
    ("", 1, 0),
    ("Diamon", 1, 0),
    ("Diamondd", 1, 0),
    ("Premiu", 1, 0),
    ("Premiumm", 1, 0),
    ("Standar", 1, 0),
    ("Standardd", 1, 0),
]


def test_program_incorrect():
    for program, input_, output_ in programConfigsIncorrect:
        assert calculateBonuses(program, input_) == output_
