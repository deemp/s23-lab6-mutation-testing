
def summ(num1, num2):
    return float(num1) + float(num2)

def div(num1, num2):
    return num1 / num2

def mult(num1, num2):
    return num1 * num2

def sub(num1, num2):
    return num1 - num2

INVALID_SIGN = "Invalid sign. Use one of +, -, *, / signs."
DIVISION_BY_ZERO = "Division by zero."

def count(num1, num2, sign):
    if sign == "+":
        res = summ(num1, num2)
    elif sign == "-":
        res = sub(num1, num2)
    elif sign == "*":
        res = mult(num1, num2)
    elif sign == "/":
        if num2 == 0:
            res = DIVISION_BY_ZERO
            return res
        res = div(num1, num2)
    else:
        res = INVALID_SIGN
    return res

